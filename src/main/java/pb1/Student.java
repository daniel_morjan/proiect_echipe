package pb1;

import java.util.Objects;

public class Student {
    private String name;
    private float average;

    public Student(String name, float average) {
        this.name = name;
        this.average = average;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", average=" + average +
                '}';
    }

    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(o==null || o.getClass()!=this.getClass())
            return false;
        Student student = (Student) o;
        return Objects.equals(name,student.getName()) && Float.compare(average,student.getAverage())==0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name,this.average);
}

    public String getName() {
        return name;
    }

    public float getAverage() {
        return average;
    }
}