package socialnetwork;


import org.javatuples.Pair;
import pb1.Student;
import pb2.MyMap;
import socialnetwork.Ui.Console;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.PrietenieValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.db.MessageDb;
import socialnetwork.repository.db.PrietenieDb;
import socialnetwork.repository.db.UserDb;
import socialnetwork.repository.file.PrietenieFileRepository;
import socialnetwork.repository.file.UserFileRepository;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.ReteaService;
import socialnetwork.service.UserService;

import java.lang.reflect.Array;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("ok");



        //repository
        Validator<User> userValidator = new UtilizatorValidator();
        Repository<Long, User> userDbRepository = new UserDb("postgres", "legenhaci", "jdbc:postgresql://localhost:5432/retea-socializare",userValidator);

        Validator<Prietenie> friendshipValidator = new PrietenieValidator();
        Repository<Pair<Integer, Integer>, Prietenie> friendshipDbRepo = new PrietenieDb("postgres", "legenhaci", "jdbc:postgresql://localhost:5432/retea-socializare",friendshipValidator);

        Repository<Long, Message> messageDbRepo = new MessageDb("postgres", "legenhaci", "jdbc:postgresql://localhost:5432/retea-socializare");



        //service
        UserService userService = new UserService(userDbRepository);
        PrietenieService friendshipService = new PrietenieService(friendshipDbRepo);
        ReteaService reteaService = new ReteaService(userService, friendshipService);
        MessageService messageService = new MessageService(messageDbRepo);

        //ui
        Console console = new Console(userService, friendshipService, reteaService,messageService);
        console.run();

//        String fileName="data/users.csv";
//        String fileName1="data/prietenii.csv";
//
//        Repository<Long, User> userFileRepository = new UserFileRepository(fileName
//                , new UtilizatorValidator());
//        Repository<Pair<Integer,Integer>, Prietenie> prietenieRepository = new PrietenieFileRepository(fileName1
//                , new PrietenieValidator());
//
//        UserService userService=new UserService(userFileRepository);
//
//        PrietenieService prietenieService=new PrietenieService(prietenieRepository);
//
//        ReteaService reteaService=new ReteaService(userService,prietenieService);
//
//        Console console = new Console(userService, prietenieService, reteaService);
//        console.run();

    }
}


