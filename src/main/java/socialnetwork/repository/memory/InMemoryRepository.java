package socialnetwork.repository.memory;

import org.javatuples.Pair;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {
    private Validator<E> validator;
    protected Map<ID, E> entities;

    public InMemoryRepository() {
    }

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<ID, E>();
    }

    @Override
    public E findOne(ID id) {
        if (id == null)
            throw new IllegalArgumentException("The id cannot be null");
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("The entity cannot be null");
        validator.validate(entity);
        entity = getEntityDacaEPrietenie(entity);
        if (entities.containsKey(entity.getId())) {
            throw new IllegalArgumentException("Entity with given id already exists !");
        }
        entities.put(entity.getId(), entity);
        return entity;
    }

    private E getEntityDacaEPrietenie(E entity) {
        if(entity instanceof Prietenie) {
            Prietenie that=(Prietenie) entity;
            int id1=that.getId().getValue0();
            int id2=that.getId().getValue1();
            if(id1>id2) {
                Pair<Integer,Integer> pair=Pair.with(id2,id1);
                that.setId(pair);
            }
        }
        return entity;
    }

    @Override
    public E delete(ID id) {
        if (entities.get(id) == null) throw new IllegalArgumentException("entity with given id dose not exist");
        E e=entities.get(id);
        entities.remove(id);
        return e;
    }

    @Override
    public E update(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        entities.put(entity.getId(), entity);

        if (entities.get(entity.getId()) != null) {
            entities.put(entity.getId(), entity);
            return null;
        }
        return entity;
    }

}
