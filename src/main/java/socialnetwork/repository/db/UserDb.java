package socialnetwork.repository.db;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class UserDb implements Repository<Long, User>{

    private final String username;
    private final String password;
    private final String url;
    private Validator<User> validator;

    /**
     * @param username the username of the database admin
     * @param password the pasword of the database admin
     * @param url the url of the database
     */
    public UserDb(String username, String password, String url, Validator<User> validator) {
        this.username = username;
        this.password = password;
        this.url = url;
        this.validator = validator;

    }


    /**
     * @param userId - the id of the user
     * @return the user with the given id
     */
    @Override
    public User findOne(Long userId) {
        if (userId == null)
            throw new IllegalArgumentException("Id must be not null !");

        String sql = "SELECT * FROM users WHERE id = (?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String mail = resultSet.getString("mail");
                String password = resultSet.getString("password");
                User user = new User(firstName, lastName,mail,password);
                user.setId(id);
                return user;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * @return all the users from the database
     */
    @Override
    public Iterable<User> findAll() {

        String sql = "SELECT * from users";
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long id = resultSet.getLong(1);
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String mail = resultSet.getString("mail");
                String password = resultSet.getString("password");
                User user = new User(firstName, lastName,mail,password);
                user.setId(id);
                users.add(user);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return users;
    }

    /**
     * @param entity entity must be not null
     * @return the user or null
     */
    @Override
    public User save(User entity) {
        if(entity==null)
        {
            throw new IllegalArgumentException("entity cannot be null");
        }
        validator.validate(entity);
        String sql = "INSERT INTO users (first_name, last_name,mail,password) VALUES (?,?,?,?) RETURNING id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setString(3, entity.getMail());
            preparedStatement.setString(4, entity.getPassword());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long id = resultSet.getLong("id");
                entity.setId(id);
                return entity;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * Delete a user with the given id from the database
     *
     * @param userId - the given user id
     * @return
     */
    @Override
    public User delete(Long userId) {
        String sql = "DELETE FROM users WHERE id = (?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, userId);

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return findOne(userId);
    }

    /**
     * Update the user with the given id with the given user date's
     *
     * @param entity entity must not be null
     * @return
     */
    @Override
    public User update(User entity) {
        String sql = "UPDATE users SET first_name = (?), last_name=(?) ,mail=(?) ,password=(?) WHERE id = (?)";
        if(entity==null)
        {
            throw new IllegalArgumentException("entity cannot be null");
        }
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setString(3, entity.getMail());
            preparedStatement.setString(4, entity.getPassword());
            preparedStatement.setLong(5, entity.getId());
            User e=findOne(entity.getId());
            preparedStatement.executeUpdate();
            return e;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }


}

