package socialnetwork.repository.db;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class MessageDb implements Repository<Long, Message> {

    private final String username;
    private final String password;
    private final String url;

    public MessageDb(String username, String password, String url) {
        this.username = username;
        this.password = password;
        this.url = url;
    }

    @Override
    public Message findOne(Long messageId) {
        if (messageId == null)
            throw new IllegalArgumentException("Id must be not null !");

        String sql = "SELECT * FROM messages WHERE id = (?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, messageId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long id = resultSet.getLong("id");
                long from = resultSet.getInt("from_user");

                String toAllList = resultSet.getString("to_user");
                String replace = toAllList.replace("[","");
                String replace1 = replace.replace("]","");
                List<String> tolist = new ArrayList<String>(Arrays.asList(replace1.split("[\\s,]+")));
                List<Long> to = tolist.stream().map(Long::parseLong).collect(Collectors.toList());

                String message = resultSet.getString("mesaj");
                long date = resultSet.getLong("data");
                int reply = resultSet.getInt("reply");

                Message mess = new Message(from, to, message, date, reply);
                mess.setId(id);
                return mess;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        String sql = "SELECT * from messages";
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long id = resultSet.getLong(1);
                long from = resultSet.getInt("from_user");

                String toAllList = resultSet.getString("to_user");
                String replace = toAllList.replace("[","");
                String replace1 = replace.replace("]","");
                List<String> tolist = new ArrayList<String>(Arrays.asList(replace1.split("[\\s,]+")));
                List<Long> to = tolist.stream().map(Long::parseLong).collect(Collectors.toList());

                String message = resultSet.getString("mesaj");
                long date = resultSet.getLong("data");
                int reply = resultSet.getInt("reply");

                Message mess = new Message(from, to, message, date, reply);
                mess.setId(id);
                messages.add(mess);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return messages;
    }

    @Override
    public Message save(Message entity) {
        String sql = "INSERT INTO messages (from_user, to_user, mesaj, data, reply) VALUES (?,?,?,?,?) RETURNING id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getFrom());
            preparedStatement.setString(2, entity.getTo().toString());
            preparedStatement.setString(3, entity.getMessage());
            preparedStatement.setLong(4, entity.getDate());
            preparedStatement.setInt(5, entity.getReply());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long id = resultSet.getLong("id");
                entity.setId(id);
                return entity;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public Message delete(Long messageId) {
        String sql = "DELETE FROM messages WHERE id = (?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, messageId);

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return findOne(messageId);
    }

    @Override
    public Message update(Message entity) {
        String sql = "UPDATE messages SET from_user = (?), to_user=(?), mesaj=(?), data=(?), reply=(?) WHERE id = (?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getFrom());
            preparedStatement.setString(2, entity.getTo().toString());
            preparedStatement.setString(3, entity.getMessage());
            preparedStatement.setLong(4, entity.getDate());
            preparedStatement.setInt(5, entity.getReply());
            preparedStatement.setLong(6, entity.getId());
            Message e=findOne(entity.getId());
            preparedStatement.executeUpdate();
            return e;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
