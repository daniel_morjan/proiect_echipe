package socialnetwork.repository.db;

import org.javatuples.Pair;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class PrietenieDb implements Repository<Pair<Integer, Integer>, Prietenie> {

    private final String username;
    private final String password;
    private final String url;
    private Validator<Prietenie> validator;

    /**
     * @param username the username of the database admin
     * @param password the pasword of the database admin
     * @param url the url of the database
     */
    public PrietenieDb(String username, String password, String url,Validator<Prietenie> validator) {
        this.username = username;
        this.password = password;
        this.url = url;
        this.validator=validator;
    }


    /**
     * @param friendshipId - the touple of id of the friendship
     * @return the frienship with the touple of id
     */
    @Override
    public Prietenie findOne(Pair<Integer, Integer> friendshipId) {
        if (friendshipId == null)
            throw new IllegalArgumentException("Id must be not null !");

        String sql = "SELECT * FROM friendship WHERE (first_user = (?) AND second_user = (?))" +
                " or (first_user = (?) AND second_user = (?))";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, friendshipId.getValue0());
            preparedStatement.setInt(2, friendshipId.getValue1());
            preparedStatement.setInt(3, friendshipId.getValue1());
            preparedStatement.setInt(4, friendshipId.getValue0());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id1 = resultSet.getInt("first_user");
                int id2 = resultSet.getInt("second_user");
                long date = resultSet.getLong("created_date");
                String status =resultSet.getString("status");
                return new Prietenie(id1, id2,date,status);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * @return all the friendships from the table
     */
    @Override
    public Iterable<Prietenie> findAll() {

        String sql = "SELECT * from friendship";
        Set<Prietenie> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id1 = resultSet.getInt(1);
                int id2 = resultSet.getInt(2);
                long date = resultSet.getLong(3);
                String status =resultSet.getString("status");
                Prietenie friendship = new Prietenie(id1, id2,date,status);
                friendships.add(friendship);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return friendships;
    }

    /**
     * @param entity entity must be not null
     * @return the entity or null
     */
    @Override
    public Prietenie save(Prietenie entity) {
        String sql = "INSERT INTO friendship (first_user, second_user, created_date ,status) VALUES (?,?,?,?)";
        if(entity==null)
        {
            throw new IllegalArgumentException("entity cannot be null");
        }
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getId().getValue0());
            preparedStatement.setLong(2, entity.getId().getValue1());
            preparedStatement.setLong(3, entity.getDate());
            preparedStatement.setString(4, entity.getStatus());
            preparedStatement.execute();

            return entity;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * Delete a frienship with the given tuple of id
     *
     * @param friendshipId - the given tuple of id
     * @return
     */
    @Override
    public Prietenie delete(Pair<Integer, Integer> friendshipId) {
        String sql = "DELETE FROM friendship WHERE (first_user = (?) AND second_user = (?))" +
                "or (first_user = (?) AND second_user = (?))";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, friendshipId.getValue0());
            preparedStatement.setInt(2, friendshipId.getValue1());
            preparedStatement.setInt(3, friendshipId.getValue1());
            preparedStatement.setInt(4, friendshipId.getValue0());

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * Update a friendship with the given touple of id and modify the date
     *
     * @param entity entity must not be null
     * @return
     */
    @Override
    public Prietenie update(Prietenie entity) {
        if(entity==null)
        {
            throw new IllegalArgumentException("entity cannot be null");
        }
        validator.validate(entity);
        String sql = "UPDATE friendship SET created_date = (?), status=(?) WHERE (first_user = (?) AND second_user = (?))"+
                "or (first_user = (?) AND second_user = (?))";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getDate());
            preparedStatement.setString(2, entity.getStatus());
            preparedStatement.setInt(3, entity.getId().getValue0());
            preparedStatement.setInt(4, entity.getId().getValue1());
            preparedStatement.setInt(5, entity.getId().getValue1());
            preparedStatement.setInt(6, entity.getId().getValue0());

            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return entity;
    }


}
