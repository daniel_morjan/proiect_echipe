package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factory class (vezi mai jos)

public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    private void loadData() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String newLine;
            while ((newLine = reader.readLine()) != null) {
                System.out.println(newLine);
                List<String> data = Arrays.asList(newLine.split(";"));
                E entity = extractEntity(data);
                super.save(entity);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * extract entity  - template method design pattern
     * creates an entity of type E having a specified list of @code attributes
     *
     * @param attributes
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     * Returns the string containing the entity.
     *
     * @param entity
     * @return
     */
    protected abstract String createEntityAsString(E entity);

    @Override
    public E save(E entity) {
        if (super.save(entity) != null) {
            writeMapToFile(entities);
        }
        return entity;
    }

    @Override
    public E delete(ID id) {
        E e = entities.get(id);
        super.delete(id);
        writeMapToFile(entities);
        return e;
    }

    public void writeMapToFile(Map<ID, E> map) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            new PrintWriter(fileName).close();
            int i = 0;
            for (Map.Entry<ID, E> pair : map.entrySet()) {
                E entiti = pair.getValue();
                entiti.setId(pair.getKey());
                i++;
                writer.write(createEntityAsString(entiti));
                if (i < map.size())
                    writer.newLine();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void writeToFile(E entity) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {

            writer.newLine();
            writer.write(createEntityAsString(entity));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
