package socialnetwork.repository.file;

import org.javatuples.Pair;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class PrietenieFileRepository extends AbstractFileRepository<Pair<Integer, Integer>, Prietenie> {
    public PrietenieFileRepository(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        Prietenie prietenie = new Prietenie();
        int id1 = Integer.parseInt(attributes.get(0));
        int id2 = Integer.parseInt(attributes.get(1));
        if (id1 < id2) {
            return getPrietenie(prietenie, id1, id2);
        }
        else {
            return getPrietenie(prietenie, id2, id1);
        }
    }

    private Prietenie getPrietenie(Prietenie prietenie, int id1, int id2) {
        Pair<Integer, Integer> pair = Pair.with(id1, id2);
        prietenie.setId(pair);
        return prietenie;
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getValue0() + ";" + entity.getId().getValue1();
    }
}
