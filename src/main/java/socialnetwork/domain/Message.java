package socialnetwork.domain;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    private long from;
    private List<Long> to;
    private String message;
    private Calendar calendar = Calendar.getInstance();
    private int reply;

    public Message(long from, List<Long> to, String message, long date, int reply) {
        this.from = from;
        this.to = to;
        this.message = message;
        calendar.setTimeInMillis(date);
        this.reply = reply;
    }

    public long getDate() {
        return calendar.getTimeInMillis();

    }

    public long getFrom() {
        return from;
    }

    public List<Long> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public int getReply() {
        return reply;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public void setTo(List<Long> to) {
        this.to = to;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setReply(int reply) {
        this.reply = reply;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return "Message{" +
                "id=" + getId() +" "+
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", data=" + formatter.format(getDate()) +
                ", reply=" + reply +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(this.getId(), message1.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(), from, to, message, calendar, reply);
    }
}

