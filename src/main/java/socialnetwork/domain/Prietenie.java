package socialnetwork.domain;


import org.javatuples.Pair;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Prietenie extends Entity<Pair<Integer,Integer>> {

    Calendar calendar = Calendar.getInstance();
    private String status;


    public Prietenie() {
    }
    public Prietenie(int id1, int id2, long date , String status) {
        this.setId(new Pair<>(id1, id2));
        calendar.setTimeInMillis(date);
        this.status = status;
    }
    public long getDate() {
        return calendar.getTimeInMillis();

    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return "Prietenie {" +
                "userul cu id-ul " + this.getId().getValue0() +
                " este prieten cu userul cu id-ul "
                + this.getId().getValue1() +" "+
                "data  " + formatter.format(getDate())+
                ", status=" + getStatus() +
                '}';
    }

    public String getStatus() {
        return status;
    }
}
