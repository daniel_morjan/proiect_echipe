package socialnetwork.domain;

import java.util.List;
import java.util.Objects;

public class User extends Entity<Long> {
    private String firstName;
    private String lastName;
    private List<User> friends;
    private String mail;
    private String password;

    public User(String firstName, String lastName, String mail, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getFriends() {
        return friends;
    }

    public String normaltoString() {
        return firstName + ' ' +
                lastName;

    }

    @Override
    public String toString() {
        System.out.println("Prieteni:");
        if (friends.size() != 0)
            for (int i = 0; i < friends.size(); i++)
                System.out.println(friends.get(i).normaltoString());
        System.out.println("Ai utilizatorului:");
        return "Utilizator{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' + "}" + "\n" +
                "---------------------------";

    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }
}