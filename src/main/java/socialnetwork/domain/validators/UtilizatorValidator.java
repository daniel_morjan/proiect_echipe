package socialnetwork.domain.validators;

import socialnetwork.domain.User;

public class UtilizatorValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String firstName = entity.getFirstName().trim();
        if (entity.getFirstName() == null) {
            throw new ValidationException("Invalid First Name");
        }
        if (firstName.isEmpty() || firstName.charAt(0) < 'A' || firstName.charAt(0) > 'Z') {
            throw new ValidationException("Invalid First Name(trebuie sa inceapa cu litera mare)");
        }

        String lastName = entity.getLastName().trim();
        if (entity.getLastName() == null) {
            throw new ValidationException("Invalid Last Name");
        }
        if (lastName.isEmpty() || lastName.charAt(0) < 'A' || lastName.charAt(0) > 'Z') {
            throw new ValidationException("Invalid Last Name(trebuie sa inceapa cu litera mare)");
        }
    }
}
