package socialnetwork.domain.validators;

import org.javatuples.Pair;
import socialnetwork.domain.Prietenie;

import java.util.Objects;

public class PrietenieValidator implements Validator<Prietenie>{
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        Pair<Integer, Integer> id = entity.getId();
        if (Objects.equals(id.getValue0(), id.getValue1())) {
            throw new ValidationException("Invalid Id");
        }
    }
}
