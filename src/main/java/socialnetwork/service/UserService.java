package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.repository.Repository;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UserService {
    private Repository<Long, User> repo;

    /**
     * @param repo the repository with the an id and with users
     */
    public UserService(Repository<Long, User> repo) {
        this.repo = repo;
    }

    /**
     * @param user the user to add
     */
    public void addUser(User user) {
        repo.save(user);
    }

    /**
     * remove an user by id
     *
     * @param id the id of the user
     */
    public void removeUser(Long id) {
        repo.delete(id);
    }

    /**
     * @param user the new user to update
     */
    public void updateUser(User user) {
        repo.update(user);
    }

    /**
     * @param id the id of the entity to be returned id must not be null
     * @return the entity with the specified id or null - if there is no entity with the given id
     */
    public User findOne(Long id) {
        return repo.findOne(id);
    }

    public User findByEmail(String mail)
    {
        Iterable<User> users= repo.findAll();

        return StreamSupport.stream(users.spliterator(), false)
                 .filter(x -> Objects.equals(x.getMail(), mail))
                 .findFirst().orElse(null);

    }

    /**
     * @return all the users
     */
    public Iterable<User> getAll() {
        return repo.findAll();
    }
}
