package socialnetwork.service;

import org.javatuples.Pair;
import socialnetwork.domain.Prietenie;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PrietenieService {
    private final Repository<Pair<Integer, Integer>, Prietenie> repo;

    /**
     * @param repo the repository with the touple of id and a friendship
     */

    public PrietenieService(Repository<Pair<Integer, Integer>, Prietenie> repo) {
        this.repo = repo;
    }

    /**
     * @param friendship the frienship to add
     */
    public void addFriendship(Prietenie friendship) {
        repo.save(friendship);
    }

    /**
     * @param id1 the id of the first user
     * @param id2 the id of the second user
     */
    public void removeFriendship(int id1, int id2) {
        this.repo.delete(new Pair<>(id1, id2));
    }

    /**
     * @param friendship the frienship to update
     */
    public void updateFriendship(Prietenie friendship) {
        repo.update(friendship);
    }

    /**
     * @return all the frienships
     */
    public Iterable<Prietenie> getAll() {
        return repo.findAll();
    }


    public Prietenie findOne(int id1, int id2){
        return this.repo.findOne(new Pair<>(id1, id2));
    }

    /**
     * Delete all fiendships with the user with the given id
     *
     * @param id the id of the deleted user and his friends
     */
    public void removeUserFriends(Long id){
        List<Prietenie> friendships = new ArrayList<>();
        for (Prietenie friendship: this.repo.findAll()) {
            if(Objects.equals(friendship.getId().getValue0(), id.intValue()) || Objects.equals(friendship.getId().getValue1(), id.intValue())){
                friendships.add(friendship);
            }
        }
        for (Prietenie friendship: friendships) {
            this.repo.delete(friendship.getId());
        }
    }

    /**
     * @return the number of conex components
     */
    public int numberConexComponents() {
        return dfs().getValue0();
    }

    /**
     * @return the list of friendships with the biggest number of conex components
     */
    public List<Prietenie> theLongestRoad() {
        return dfs().getValue1();
    }

    /**
     * @return a touple with an int that represents the number of conex components and a list with friendships withe the biggest number of conex components
     */
    private Pair<Integer, List<Prietenie>> dfs() {
        Iterable<Prietenie> friendships = repo.findAll();
        System.out.println("Astea-s: " + friendships);
        boolean isEmpty = !friendships.iterator().hasNext();
        if (isEmpty) {
            return new Pair<>(1, new ArrayList<>());
        }

        int numberOfConexComponents = 1;
        List<Prietenie> visited = conexComponent(friendships.iterator().next(), friendships);
        List<Prietenie> longestVisited = visited;

        List<Prietenie> globalVisited = new ArrayList<>(visited);
        for (Prietenie friendship : friendships) {
            if (!globalVisited.contains(friendship)) {
                visited = conexComponent(friendship, friendships);
                if (visited.size() > longestVisited.size()) {
                    longestVisited = visited;
                }
                globalVisited.addAll(visited);
                numberOfConexComponents++;
            }
        }
        return new Pair<>(numberOfConexComponents, longestVisited);
    }

    /**
     * @param first the first friendship from where starts the search
     * @param friendships the list of all friendships
     * @return a list with conex components strarts from first
     */
    private List<Prietenie> conexComponent(Prietenie first, Iterable<Prietenie> friendships) {
        Stack<Prietenie> stack = new Stack<>();
        List<Prietenie> visited = new ArrayList<>();
        stack.push(first);
        while (!stack.isEmpty()) {
            Prietenie current = stack.pop();
            int left = current.getId().getValue0();
            int right = current.getId().getValue1();
            if (!visited.contains(current)) {
                visited.add(current);
                for (Prietenie friendship : friendships) {
                    int left1 = friendship.getId().getValue0();
                    int right1 = friendship.getId().getValue1();
                    if (!visited.contains(friendship) && (Objects.equals(left, left1) || Objects.equals(left, right1) ||
                            Objects.equals(right, left1) || Objects.equals(right, right1))) {
                        stack.push(friendship);
                    }
                }
            }
        }
        return visited;
    }
    public List<Prietenie> getAllFriendsRequest(Long userid){
        return StreamSupport
                .stream(this.getAll().spliterator(), false)
                .filter(x -> x.getId().getValue0() == userid.intValue() || x.getId().getValue1()==userid.intValue())
                .collect(Collectors.toList());

    }

}
