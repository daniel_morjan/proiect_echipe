package socialnetwork.service;

import org.javatuples.Pair;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.User;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ReteaService {
    private final UserService userService;
    private final PrietenieService prietenieService;

    /**
     * @param userService      all the user's services
     * @param prietenieService all the user's friendship services
     */
    public ReteaService(UserService userService, PrietenieService prietenieService) {
        this.userService = userService;
        this.prietenieService = prietenieService;
        InitializareListaPrieteni();
    }

    /**
     * remove the user with the given id and his frienship list
     *
     * @param id removed user's id
     */
    public void removeUserAndUserFriends(Long id) {
        this.userService.removeUser(id);
        this.prietenieService.removeUserFriends(id);
        InitializareListaPrieteni();
    }

    public Iterable<User> InitializareListaPrieteni() {
        Iterable<User> users = this.userService.getAll();
        for (User user : users) {
            List<User> prieteni = new ArrayList<>();
            for (Prietenie prietenie : this.prietenieService.getAll()) {
                if (Objects.equals(prietenie.getId().getValue0(), user.getId().intValue())) {
                    prieteni.add(this.userService.findOne((long) prietenie.getId().getValue1()));
                }
                if (Objects.equals(prietenie.getId().getValue1(), user.getId().intValue())) {
                    prieteni.add(this.userService.findOne((long) prietenie.getId().getValue0()));
                }
            }
            user.setFriends(prieteni);
        }
        return users;
    }

    public void InitializareListaPrieteniForOneUser(User u) {
        List<User> prieteni = new ArrayList<>();
        for (Prietenie prietenie : this.prietenieService.getAll()) {
            if (Objects.equals(prietenie.getId().getValue0(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue1()));
            }
            if (Objects.equals(prietenie.getId().getValue1(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue0()));
            }
        }
        u.setFriends(prieteni);
    }

    public List<Pair<User, Long>> RelatiiPrietenieUtilizatorCitit(User u) {
        List<User> prieteni = new ArrayList<>();
        for (Prietenie prietenie : this.prietenieService.getAll()) {
            if (Objects.equals(prietenie.getId().getValue0(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue1()));
            }
            if (Objects.equals(prietenie.getId().getValue1(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue0()));
            }
        }
        Comparator<Pair<User, Long>> comp = Comparator.comparing(Pair::getValue1);
        List<Pair<User, Long>> lista = prieteni.stream().
                map(x -> new Pair<User, Long>(x, this.prietenieService.findOne(x.getId().intValue(), u.getId().intValue()).getDate())
                ).
                filter(x->
                        { Prietenie p=this.prietenieService.findOne(x.getValue0().getId().intValue(),u.getId().intValue());
                            return Objects.equals(p.getStatus(), "approved");}
                ).
                sorted(comp).
                collect(Collectors.toList());
        return lista;
    }

    public List<Pair<User, Long>> RelatiiPrietenieUtilizatorCititIntroLuna(User u, int luna) {
        List<User> prieteni = new ArrayList<>();
        for (Prietenie prietenie : this.prietenieService.getAll()) {
            if (Objects.equals(prietenie.getId().getValue0(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue1()));
            }
            if (Objects.equals(prietenie.getId().getValue1(), u.getId().intValue())) {
                prieteni.add(this.userService.findOne((long) prietenie.getId().getValue0()));
            }
        }
        Comparator<Pair<User, Long>> comp = Comparator.comparing(Pair::getValue1);
        List<Pair<User, Long>> lista = prieteni.stream().
                map(x -> new Pair<User, Long>(x, this.prietenieService.findOne(x.getId().intValue(), u.getId().intValue()).getDate())
                )
                .filter(x -> {
                            SimpleDateFormat formatter = new SimpleDateFormat("MM");
                            int dateMonth = Integer.parseInt(formatter.format(x.getValue1()));
                            return dateMonth == luna;
                        }
                ).
                filter(x->
                        { Prietenie p=this.prietenieService.findOne(x.getValue0().getId().intValue(),u.getId().intValue());
                            return Objects.equals(p.getStatus(), "approved");}
                ).
                sorted(comp).
                collect(Collectors.toList());
        return lista;
    }
}
