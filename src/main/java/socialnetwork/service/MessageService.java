package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.repository.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageService {
    private final Repository<Long, Message> repo;

    public MessageService(Repository<Long, Message> repo) {
        this.repo = repo;
    }

    public void addMessage(Message message){
        repo.save(message);
    }

    public void removeMessage(Long id){
        repo.delete(id);
    }

    public void updateMessage(Message message){
        repo.update(message);
    }

    public Message findOne(Long id)
    {
        return repo.findOne(id);
    }


    public List<Message> getAllReceivedMessages(long id){
        return StreamSupport
                .stream(this.getAll().spliterator(), false)
                .filter(x -> x.getTo().contains(id))
                .collect(Collectors.toList());
    }

    public List<Message> getAllGivenMessages(long id){
        return StreamSupport
                .stream(this.getAll().spliterator(), false)
                .filter(x -> x.getFrom() == id)
                .collect(Collectors.toList());
    }

    public List<Message> messagesTwoUsersCronological(long id1, long id2){
        return StreamSupport
                .stream(this.getAll().spliterator(), false)
                .filter(x -> x.getFrom() == id1 || x.getFrom() == id2)
                .filter(x -> x.getTo().size() == 1 && (x.getTo().get(0) == id1 || x.getTo().get(0) == id2))
                .sorted(Comparator.comparing(Message::getDate))
                .collect(Collectors.toList());
    }

    public Iterable<Message> getAll(){
        return repo.findAll();
    }
}
