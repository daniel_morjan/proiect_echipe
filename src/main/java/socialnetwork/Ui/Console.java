package socialnetwork.Ui;

import org.javatuples.Pair;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.User;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.ReteaService;
import socialnetwork.service.UserService;

import java.security.KeyException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class Console {
    private final UserService userService;
    private final PrietenieService friendshipService;
    private final ReteaService userFriendshipsService;
    private final MessageService messageService;

    public Console(UserService userService, PrietenieService friendshipService,
                   ReteaService userFriendshipsService, MessageService messageService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.userFriendshipsService = userFriendshipsService;
        this.messageService = messageService;
    }

    public void runUserOperations() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Add user");
            System.out.println("(2) Delete user");
            System.out.println("(3) Update user");
            System.out.println("(4) Get all users");
            System.out.println("(5) Find by id");
            System.out.println("(6) Find friendships and dates from one user");
            System.out.println("(7) Find friendships and dates from one user in a month");
            System.out.println("(0) Go back");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    try {
                        System.out.println("Frist name:");
                        String firstName = scanner.next();
                        System.out.println("Last name:");
                        String lastName = scanner.next();
                        System.out.println("Mail:");
                        String mail = scanner.next();
                        System.out.println("Password:");
                        String password = scanner.next();
                        User user = new User(firstName, lastName, mail, password);
                        this.userService.addUser(user);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        User user = userService.findOne(id);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        userFriendshipsService.removeUserAndUserFriends(id);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "3":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        System.out.println("Frist name:");
                        String firstName = scanner.next();
                        System.out.println("Last name:");
                        String lastName = scanner.next();
                        System.out.println("Mail:");
                        String mail = scanner.next();
                        System.out.println("Password:");
                        String password = scanner.next();
                        User user = new User(firstName, lastName, mail, password);
                        user.setId(id);
                        this.userService.updateUser(user);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "4":
                    try {
                        this.userFriendshipsService.InitializareListaPrieteni().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "5":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        User u = this.userService.findOne(id);
                        this.userFriendshipsService.InitializareListaPrieteniForOneUser(u);
                        System.out.println(u);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "6":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        User u = this.userService.findOne(id);
                        if (u == null) throw new KeyException("The user with the given id does not exists");
                        List<Pair<User, Long>> lista = this.userFriendshipsService.RelatiiPrietenieUtilizatorCitit(u);
                        for (Pair<User, Long> pair : lista) {
                            User u1 = pair.getValue0();
                            long l = pair.getValue1();
                            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            System.out.println(u1.getFirstName() + " " + u1.getLastName() + " " + formatter.format(l));
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "7":
                    try {
                        System.out.println("User id:");
                        Long id = scanner.nextLong();
                        System.out.println("Luna:");
                        int luna = scanner.nextInt();
                        User u = this.userService.findOne(id);
                        if (u == null) throw new KeyException("The user with the given id does not exists");
                        List<Pair<User, Long>> lista =
                                this.userFriendshipsService.RelatiiPrietenieUtilizatorCititIntroLuna(u, luna);
                        for (Pair<User, Long> pair : lista) {
                            User u1 = pair.getValue0();
                            long l = pair.getValue1();
                            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            System.out.println(u1.getFirstName() + " " + u1.getLastName() + " " + formatter.format(l));
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void runFriendshipOperations() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Add frindship");
            System.out.println("(2) Delete frindship");
            System.out.println("(3) Update frindship");
            System.out.println("(4) Get all frindships");
            System.out.println("(5) Find a friendship by id");
            System.out.println("(6) Display number of comunities");
            System.out.println("(7) Display the most sociable comunity");
            System.out.println("(0) Back");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    try {
                        System.out.println("Id1:");
                        int id1 = scanner.nextInt();
                        User user = userService.findOne((long) id1);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        System.out.println("Id2: ");
                        int id2 = scanner.nextInt();
                        System.out.println("Status: ");
                        String status = scanner.next();
                        user = userService.findOne((long) id2);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        if (id1 > id2) {

                            Prietenie p = new Prietenie(id2, id1, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), status);

                            this.friendshipService.addFriendship(p);
                        } else {
                            Prietenie p = new Prietenie(id1, id2, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), status);
                            this.friendshipService.addFriendship(p);
                        }

                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    try {
                        System.out.println("Id1:");
                        int id1 = scanner.nextInt();
                        User user = userService.findOne((long) id1);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        System.out.println("Id2: ");
                        int id2 = scanner.nextInt();
                        user = userService.findOne((long) id2);
                        if (user == null) throw new KeyException("The user with the given id does not exists");
                        if (id1 > id2) {
                            this.friendshipService.removeFriendship(id2, id1);
                        } else {
                            this.friendshipService.removeFriendship(id1, id2);
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "3":
                    try {
                        System.out.println("Id1: ");
                        int id1 = scanner.nextInt();
                        System.out.println("Id2: ");
                        int id2 = scanner.nextInt();
                        System.out.println("Status: ");
                        String status = scanner.next();
                        if (id1 > id2) {

                            Prietenie p = new Prietenie(id2, id1, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), status);

                            this.friendshipService.updateFriendship(p);
                        } else {
                            Prietenie p = new Prietenie(id1, id2, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), status);
                            this.friendshipService.updateFriendship(p);
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "4":
                    try {
                        this.friendshipService.getAll().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "5":
                    try {
                        System.out.println("Id1: ");
                        int id1 = scanner.nextInt();
                        System.out.println("Id2: ");
                        int id2 = scanner.nextInt();
                        System.out.println(this.friendshipService.findOne(id1, id2));
                    } catch (Exception e) {
                        System.out.println(e);
                        e.printStackTrace();

                    }
                    break;
                case "6":
                    try {
                        System.out.println("The number of conex components: " + friendshipService.numberConexComponents());
                    } catch (Exception e) {
                        System.out.println(e);
                        e.printStackTrace();

                    }
                    break;
                case "7":
                    try {
                        System.out.println("The most social community is: " + friendshipService.theLongestRoad());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void runMessageOperations() {
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Add message");
            System.out.println("(2) Delete message");
            System.out.println("(3) Update message");
            System.out.println("(4) Get all messages");
            System.out.println("(5) Find a message by id");
            System.out.println("(6) Show messages of two users");
            System.out.println("(0) Back");

            System.out.println("Command:");
            String command = scanner.next();

            switch (command) {
                case "1":
                    try {
                        System.out.println("From:");
                        long from = scanner.nextInt();

                        System.out.println("To:");
                        String toString = scanner.next();
                        List<String> tolist = new ArrayList<String>(Arrays.asList(toString.split(" ")));
                        List<Long> to = tolist.stream().map(Long::parseLong).collect(Collectors.toList());

                        System.out.println("Message:");
                        String message = scanner.next();

                        int reply = 0;
                        System.out.println("Do you want to reply to anyone ?(y/n)");
                        if (Objects.equals(scanner.next(), "y")) {
                            System.out.println("Reply:");
                            reply = scanner.nextInt();
                        }

                        this.messageService.addMessage(new Message(from, to, message, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), reply));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "2":
                    try {
                        System.out.println("Id: ");
                        long id = scanner.nextLong();
                        this.messageService.removeMessage(id);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "3":
                    try {
                        System.out.println("From:");
                        long from = scanner.nextInt();

                        System.out.println("To:");
                        String toString = scanner.next();
                        List<String> tolist = new ArrayList<String>(Arrays.asList(toString.split(" ")));
                        List<Long> to = tolist.stream().map(Long::parseLong).collect(Collectors.toList());

                        System.out.println("Message:");
                        String message = scanner.next();
                        System.out.println("Do you want to reply to anyone ?(y/n)");
                        int reply = 0;
                        if (Objects.equals(scanner.next(), "y")) {
                            System.out.println("Reply:");
                            reply = scanner.nextInt();
                        }

                        this.messageService.updateMessage(new Message(from, to, message, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), reply));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "4":
                    try {
                        this.messageService.getAll().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "5":
                    try {
                        System.out.println("Id:");
                        long id = scanner.nextLong();
                        System.out.println(this.messageService.findOne(id));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                    }
                    break;
                case "6":
                    try {
                        System.out.println("Id1:");
                        long id1 = scanner.nextLong();
                        System.out.println("Id2:");
                        long id2 = scanner.nextLong();
                        messageService.messagesTwoUsersCronological(id1, id2).forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;

                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void runadmin() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) User Operatinos");
            System.out.println("(2) Frienship Operation");
            System.out.println("(3) Message Operation");
            System.out.println("(0) Exit");

            System.out.println("Command:");
            String command = scanner.next();
            switch (command) {
                case "1":
                    runUserOperations();
                    break;
                case "2":
                    runFriendshipOperations();
                    break;
                case "3":
                    runMessageOperations();
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("_______________________");

            System.out.println("mail");
            String mail = scanner.next();
            System.out.println("password");
            String password = scanner.next();
            try {
                if (Objects.equals(mail, "admin") && Objects.equals(password, "admin"))
                    runadmin();
                else {
                    User user = userService.findByEmail(mail);
                    if (user == null)
                        throw new KeyException("The email does not exist");

                    if (Objects.equals(user.getPassword(), password)) {
                        runUser(user);
                    } else
                        throw new KeyException("Incorrect password");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }

    private void runUser(User user) {
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        while (true) {
            System.out.println("_______________________");
            System.out.println("(1) Send message");
            System.out.println("(2) Delete message");
            System.out.println("(3) Send friend request");
            System.out.println("(4) Accept friend request");
            System.out.println("(5) Decline friend request");
            System.out.println("(6) Show all users");
            System.out.println("(7) Show received messages");
            System.out.println("(8) Show given messages");
            System.out.println("(9) Show all friend request");
            System.out.println("(10) Remove friend");
            System.out.println("(0) Back");

            System.out.println("Command:");
            String command = scanner.next();
            switch (command) {
                case "1":
                    try {
                        System.out.println("Do you want to reply to anyone ?(y/n)");
                        if (Objects.equals(scanner.next(), "y")) {
                            System.out.println("La ce mesaj vreti sa raspundeti:");
                            int reply = scanner.nextInt();
                            Message m = messageService.findOne((long) reply);
                            if (m == null)
                                throw new KeyException("nu aveti acest mesaj");
                            if (!Objects.equals(m.getTo().get(0), user.getId()))
                                throw new KeyException("nu aveti acest mesaj");
                            System.out.println("Mesajul dvs:");
                            String message = scanner.next();
                            List<Long> to = new ArrayList<>();
                            to.add(m.getFrom());
                            this.messageService.addMessage(new Message(user.getId(), to, message, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), reply));


                        } else {
                            System.out.println("Email/s:");
                            String email = scanner.next();
                            int reply = 0;
                            List<String> emailList = new ArrayList<>(Arrays.asList(email.split(" ")));
//                            List<Long> to = emailList.stream().map(x -> {
//                                try {
//                                    return userService.findByEmail(x).getId();
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                                return null;
//                            }).collect(Collectors.toList());
                            List<Long> to = new ArrayList<>();
                            int k = 0;
                            for (String s : emailList) {
                                if (userService.findByEmail(s) != null) {
                                    to.add(userService.findByEmail(s).getId());
                                    k = k + 1;
                                }
                            }
                            if (k == 0)
                                throw new KeyException("niciun mail nu este corect");
                            System.out.println("Message:");
                            String message = scanner.next();

                            this.messageService.addMessage(new Message(user.getId(), to, message, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), reply));
                        }
                        ;
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "2":
                    try {
                        System.out.println("Id:");
                        long id = scanner.nextLong();
                        Message m = messageService.findOne(id);
                        if (m == null || m.getFrom() != user.getId())
                            throw new Exception("Nu puteti sterge mesajele altora");
                        else
                            this.messageService.removeMessage(id);

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "3":
                    try {
                        System.out.println("Email:");
                        String email = scanner.next();
                        User user1 = userService.findByEmail(email);
                        int to;
                        if (user1 == null)
                            throw new Exception("This email does not exist");
                        else
                            to = user1.getId().intValue();
                        Prietenie friendship = friendshipService.findOne(user.getId().intValue(), to);
                        if (friendship == null)
                            this.friendshipService.addFriendship(new Prietenie(user.getId().intValue(), to, LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), "pending"));
                        else
                            throw new Exception("Request already sent or existing friendship");
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "4":
                    try {
                        System.out.println("Email:");
                        String email = scanner.next();
                        User user1 = userService.findByEmail(email);
                        int to;
                        if (user1 == null)
                            throw new Exception("This email does not exist");
                        else
                            to = user1.getId().intValue();
                        Prietenie friendship = friendshipService.findOne(to, user.getId().intValue());
                        if (friendship != null && Objects.equals(friendship.getStatus(), "pending"))
                            this.friendshipService.updateFriendship(new Prietenie(to, user.getId().intValue(), LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), "approved"));
                        else
                            throw new Exception("Request does not exist");

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "5":
                    try {
                        System.out.println("Email:");
                        String email = scanner.next();
                        User user1 = userService.findByEmail(email);
                        int to;
                        if (user1 == null)
                            throw new Exception("This email does not exist");
                        else
                            to = user1.getId().intValue();
                        Prietenie friendship = friendshipService.findOne(to, user.getId().intValue());
                        if (friendship != null && Objects.equals(friendship.getStatus(), "pending"))
                            this.friendshipService.updateFriendship(new Prietenie(to, user.getId().intValue(), LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), "declined"));
                        else
                            throw new Exception("Request does not exist");

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "6":
                    try {
                        this.userFriendshipsService.InitializareListaPrieteni().forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                    }
                    break;
                case "7":
                    try {
                        for (Message message : messageService.getAllReceivedMessages(user.getId()))
                            System.out.println(message);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "8":
                    try {
                        for (Message message : messageService.getAllGivenMessages(user.getId()))
                            System.out.println(message);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "9":
                    try {
                        friendshipService.getAllFriendsRequest(user.getId()).forEach(System.out::println);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "10":
                    try {
                        System.out.println("Email:");
                        String email = scanner.next();
                        User user1 = userService.findByEmail(email);
                        int to;
                        if (user1 == null)
                            throw new Exception("This email does not exist");
                        else
                            to = user1.getId().intValue();
                        Prietenie friendship = friendshipService.findOne(to, user.getId().intValue());
                        Prietenie friendship1 = friendshipService.findOne(user.getId().intValue(), to);
                        if (friendship != null && Objects.equals(friendship.getStatus(), "approved"))
                            this.friendshipService.removeFriendship(to, user.getId().intValue());
                        else if (friendship1 != null && Objects.equals(friendship1.getStatus(), "approved"))
                            this.friendshipService.removeFriendship(user.getId().intValue(), to);
                        else
                            throw new Exception("The frienship does not exist");

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;

                case "0":
                    return;
                default:
                    System.out.println("Wrong command");
            }
        }
    }
}
