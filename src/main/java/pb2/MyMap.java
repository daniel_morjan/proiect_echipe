package pb2;

import pb1.Student;

import java.util.*;

public class MyMap {
    public static class KeyComparator implements Comparator<Integer>{
        @Override
        public int compare(Integer i1, Integer i2){
            if(i1 < i2)
                return 1;
            if(i1==i2)
                return 0;
            return -1;
        }
    }
    public void add(Student student){
        int media = Math.round(student.getAverage());
        List<Student> lista = map.get(media);
        if(lista!=null){
            lista.add(student);
            map.put(media, lista);
        }
        else{
            List<Student> newLista = new ArrayList<Student>();
            newLista.add(student);
            map.put(media,newLista);
        }
    }

    public void printAll(){
        for(Map.Entry<Integer, List<Student>> pairStudents: map.entrySet()){
            System.out.println("key="+pairStudents.getKey()+" Value "+pairStudents.getValue());
        }
    }

    public MyMap() {
        map = new TreeMap<Integer, List<Student>>();
    }

    Map<Integer, List<Student>> map;
}
